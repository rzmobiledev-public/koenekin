.DEFAULT_GOAL=help

# Variables
MAKE				:= make --no-print-directory

create-requirements: ## exporting poetry installation to dev requirements.txt
	@cd backend; poetry export  --without-hashes -f requirements.txt --output installer/requirements.txt --with dev

migrations: ## Runing makemigration and migrate database
	@cd backend; python manage.py makemigrations
	@cd backend; python manage.py migrate

up: ## Run docker compose up for all projects
	@docker compose -f docker-compose.yaml --profile default up --build --force-recreate -d

down: ## Run docker compose down for all projects
	@docker compose -f docker-compose.yaml --profile default down

run-local: ## Running flask in local
	@cd backend; python main.py

.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'