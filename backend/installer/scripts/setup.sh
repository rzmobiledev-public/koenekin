#!/bin/bash

set -e

python manage.py wait_for_db --settings=config.settings
python manage.py makemigrations --settings=config.settings
python manage.py migrate --settings=config.settings

echo "======================================"
echo "KOENEKIN: YOUR PROJECT IS READY TO GO!"
echo "======================================"

export DJANGO_SETTINGS_MODULE=config.settings

python main:app --host 0.0.0.0 --port 5000