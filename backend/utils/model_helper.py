from typing import Union
from django.db.models import Model


def get_model_or_none(model: Model, **kwargs) -> Union[Model, None]:
    try:
        return model.objects.get(**kwargs)
    except model.DoesNotExist:
        return None


def filter_latest_or_none(model: Model, **kwargs) -> Union[Model, None]:
    data = model.objects.filter(**kwargs)

    if data.exists():
        return data.latest("created_at")
