from typing import Any, Sequence, List


def is_error_dict_bad_request(errors: Sequence[Any]) -> bool:
    """Check if the given error indicates a malformed request."""
    if not len(errors) == 1:
        return False

    error_item = errors[0]

    if not isinstance(error_item, dict):
        return False

    if not isinstance(error_item.get("loc"), list):
        return False

    loc = error_item["loc"]

    if not 1 <= len(loc) <= 2:
        return False

    loc_item1 = loc[0]

    if loc_item1 != "body":
        return False

    loc_item2 = loc[1] if len(loc) > 1 else None

    if loc_item2:
        return False

    if not isinstance(error_item.get("msg"), str):
        return False

    msg = error_item["msg"]

    return (
        msg == "field required"
        or msg == "value is not a valid dict"
        or msg.startswith("Expecting value:")
    )


def custom_error_message(errors: Sequence[Any]) -> str:
    """
    Formating error message from fastapi
    """
    msg: dict = {}

    if isinstance(errors, List):
        for error in errors:
            msg["detail"] = f"{error['loc'][-1]} - {error['msg']}"

    return msg
