from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from fastapi import Request, status
from fastapi.encoders import jsonable_encoder
from fastapi.openapi.utils import get_openapi

from utils.error_helpers import custom_error_message
from api import users_router

from config import settings
app = FastAPI()

# Middlewares
if not settings.DEBUG:
    app.add_middleware(
        CORSMiddleware,
        allow_origins=settings.ALLOWED_ORIGINS,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )


@app.exception_handler(RequestValidationError)
async def request_validation_error_handler(
    request: Request, exc: RequestValidationError
) -> JSONResponse:
    errors = jsonable_encoder(exc.errors())
    status_code = (
        status.HTTP_400_BAD_REQUEST
        # if is_error_dict_bad_request(errors)
        # else status.HTTP_400_BAD_REQUEST
    )

    return JSONResponse(
        status_code=status_code,
        content=custom_error_message(errors),
    )

app.include_router(users_router)


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Soursage Backend",
        version="1.1.0",
        description="Konekin - Connecting the dots",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi
