from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from .managers import AppUserManager
from rest_framework_simplejwt.tokens import RefreshToken


class TimeStamped(models.Model):
    created_at = models.DateField(auto_now_add=True, editable=False)
    updated_at = models.DateField(auto_now=True, editable=True)

    class Meta:
        abstract = True

    objects = AppUserManager()


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(("email address"), unique=True)
    username = models.CharField(max_length=255, unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    USERNAME_FIELD = "username"
    AUTH_FIELD_NAME = "email"
    REQUIRED_FIELDS = ["email"]

    class Meta:
        db_table = "users"
        verbose_name = "user"
        verbose_name_plural = "Users"

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):

        if self._password is not None:
            self.set_password(self.password)

        super().save(*args, **kwargs)

    @property
    def jwt_token(self) -> str:
        refresh = RefreshToken.for_user(self)
        return str(refresh.access_token)


class Products(TimeStamped):
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="user_products",
    )
    name = models.CharField(max_length=250, null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    logo_id = models.ImageField()

    class Meta:
        db_table = "products"
        verbose_name = "product"
        verbose_name_plural = "Products"


class VariantImages(TimeStamped):
    name = models.CharField(max_length=250, null=True, blank=True)
    image = models.ImageField(upload_to="media/", null=True, blank=True)

    class Meta:
        ordering = ["id"]
        abstract = False

    def __str__(self):
        return self.name


class Variants(TimeStamped):
    product_id = models.ForeignKey(
        Products,
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name="variant_products",
    )
    name = models.CharField(max_length=250, null=True, blank=True)
    size = models.CharField(max_length=10, null=True, blank=True)
    color = models.CharField(max_length=100, null=True, blank=True)
    images = models.ManyToManyField(VariantImages, related_name="variants")

    class Meta:
        db_table = "variant"
        verbose_name = "variant"
        verbose_name_plural = "Variants"

    def __str__(self):
        return self.name
