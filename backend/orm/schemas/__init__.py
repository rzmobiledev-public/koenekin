

from fastapi.exceptions import HTTPException
from rest_framework_simplejwt.tokens import AccessToken, RefreshToken
from rest_framework_simplejwt.exceptions import TokenError
from passlib.handlers.django import django_pbkdf2_sha256 as decrypt
from django.contrib.auth.hashers import make_password

from pydantic import BaseModel, Field
from typing import Optional
from datetime import datetime

from utils.model_helper import get_model_or_none
from utils.check_email import check_email


from orm.models import (User)


class BaseTimestampedSchema(BaseModel):
    created_at: datetime = datetime
    updated_at: datetime = datetime


class UserListSchema(BaseTimestampedSchema):
    email: str
    username: str
    is_staff: bool
    is_active: bool

    class Config:
        orm_mode = True


class UserSchema(BaseModel):
    email: str
    username: str = Field(..., min_length=3)
    password: str = Field(..., min_length=5)
    is_staff: Optional[bool]
    is_active: Optional[bool]

    class Config:
        orm_mode = True

    def __str__(self):
        self.email

    def create(self):
        try:
            if not check_email(self.email):
                raise HTTPException(
                    status_code=404, detail="Email not valid email address"
                )
            user, created = User.objects.get_or_create(email=self.email, username=self.username,
                                                       is_active=self.is_active, is_staff=self.is_staff,)
            if created:
                user.password = make_password(self.password)
                user.save()
        except User.DoesNotExist:
            raise HTTPException(
                status_code=404, detail="User not found in our database"
            )

        return user

    @classmethod
    def get(cls, **kwargs):
        if user := get_model_or_none(User, **kwargs):
            return cls.from_orm(user)


class UserLoginSchemaResult(BaseModel):
    token: str


class UserLoginSchema(BaseModel):
    email: str
    password: str

    class Config:
        orm_mode = True

    def post(self):
        try:
            if user := get_model_or_none(User, email=self.email):
                if check_password := decrypt.verify(self.password, user.password):
                    return {"token": user.jwt_token}

            raise HTTPException(
                status_code=404, detail="Password or email is incorrect"
            )

        except KeyError:
            pass

        except TokenError:
            raise HTTPException(status_code=401, detail="Your token is expired")


class Products(BaseTimestampedSchema):
    user: UserSchema
    name: str
    description: str
    logo_id = str
