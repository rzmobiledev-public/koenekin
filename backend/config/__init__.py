from pydantic import BaseSettings, Field
from typing import List
from dotenv import load_dotenv

load_dotenv()


class DbSettings(BaseSettings):
    NAME: str = Field(..., env="DB_NAME")
    HOST: str = Field(..., env="DB_HOST")
    PORT: int = Field(..., env="DB_PORT")
    USER: str = Field(..., env="DB_USER")
    PASSWORD: str = Field(..., env="DB_PASS")
    SECRET_KEY: str = Field(..., env="SECRET_KEY")
    DEBUG: bool = Field(True, env="DEBUG")

    ALLOWED_ORIGINS: List[str] = [
        "http://localhost:5000",
    ]

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = DbSettings()
