import os
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
django.setup()

from fastapi import APIRouter, HTTPException, status, Depends
from fastapi.security import OAuth2PasswordBearer
from utils.jwt_user import JWTBearer

from orm.schemas import (UserListSchema, UserSchema,
                         UserLoginSchema, UserLoginSchemaResult,)

users_router = APIRouter(prefix="/user", tags=["User"])


@users_router.get("/{user_id}", dependencies=[Depends(JWTBearer())], response_model=UserListSchema, status_code=status.HTTP_200_OK,)
def get_user(user_id: int):

    data = UserSchema.get(pk=user_id)
    if not data:
        raise HTTPException(status_code=404, detail="User not found")

    return data


@users_router.post(
    "",
    response_model=UserListSchema,
    status_code=status.HTTP_201_CREATED,
)
def add_new_user(
    user: UserSchema,
):
    return user.create()


@users_router.post(
    "/login",
    response_model=UserLoginSchemaResult, status_code=status.HTTP_200_OK,)
def login(user: UserLoginSchema = Depends()):
    return user.post()
